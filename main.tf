provider "aws" {
  region     = "us-east-1"
  access_key = "AKIARKNI37JH6DVMVDR3"
  secret_key = "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"
}

variable "availability_zone" {
  description = "availability zone"
}

variable "vpc_cidr_block" {
  description = "vpc cidr block"
}

variable "subnet_cidr_block" {
  description = "subnet cidr block"
}

variable "existing_subnet_cidr_block" {
  description = "existing subnet cidr block"
}

resource "aws_vpc" "development-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name: "development",
    vpc_env: "dev"
  }
}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id = aws_vpc.development-vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = var.availability_zone
  tags = {
    Name: "subnet-1-dev",
  }
}

data "aws_vpc" "existing-vpc" {
  default = true
}

resource "aws_subnet" "dev-subnet-2" {
  vpc_id = data.aws_vpc.existing-vpc.id
  cidr_block = var.existing_subnet_cidr_block
  availability_zone = var.availability_zone
  tags = {
    Name: "subnet-2-default"
  }
}
